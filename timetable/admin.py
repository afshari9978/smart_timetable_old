from django.contrib import admin
from timetable.models import Professor, WeekPlan, Plan, God
# Register your models here.

admin.site.register(God)
admin.site.register(Professor)
admin.site.register(WeekPlan)
admin.site.register(Plan)