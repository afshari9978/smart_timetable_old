from django.db import models


# TODO: auto fill date & time for plan from week plan


class God(models.Model):
    logo = models.ImageField()
    about_text = models.TextField()


class Plan(models.Model):
    state = models.CharField(max_length=255,
                             choices=(("available", "حضور"), ("class", "کلاس"), ("not available", "خارج از دانشکده"),
                                      ("do not disturb", "عدم حضور"), ("meeting", "جلسه"), ("raw", "خام")),
                             default="not available")
    week_day = models.CharField(max_length=255, choices=(
        ("0shanbe", "شنبه"), ("1shanbe", "یکشنبه"), ("2shanbe", "دوشنبه"), ("3shanbe", "سه شنبه"),
        ("4shanbe", "چهارشنبه"), ("5shanbe", "پنج شنبه"), ("6shanbe", "جمعه")),
                                default="0shanbe")
    start_time = models.CharField(max_length=255, choices=(
        ("6", "۶"), ("7", "۷"), ("8", "۸"), ("9", "۹"), ("10", "۱۰"), ("11", "۱۱"), ("12", "۱۲"), ("13", "۱۳"),
        ("14", "۱۴"), ("15", "۱۵"), ("16", "۱۶"), ("17", "۱۷"), ("18", "۱۸"), ("19", "۱۹"), ("20", "۲۰"), ("21", "۲۱"),
        ("22", "۲۲")), default="8")
    color_code = models.CharField(max_length=255, blank=True)
    text = models.CharField(max_length=255)
    is_locked = models.BooleanField(blank=False)

    # if Available
    schedules_accept_count = models.IntegerField(blank=True, default="0")
    schedules_filled_count = models.IntegerField(blank=True, default="0")

    def __str__(self):
        return self.text


class WeekPlan(models.Model):
    text = models.CharField(max_length=255)  # nim sal chandom
    professor = models.CharField(max_length=255, default="")
    p_0shanbe_08 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_08")
    p_0shanbe_10 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_10")
    p_0shanbe_12 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_12")
    p_0shanbe_14 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_14")
    p_0shanbe_16 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_16")
    p_0shanbe_18 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_18")
    p_0shanbe_20 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_0shanbe_20")
    p_1shanbe_08 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_08")
    p_1shanbe_10 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_10")
    p_1shanbe_12 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_12")
    p_1shanbe_14 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_14")
    p_1shanbe_16 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_16")
    p_1shanbe_18 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_18")
    p_1shanbe_20 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_1shanbe_20")
    p_2shanbe_08 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_08")
    p_2shanbe_10 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_10")
    p_2shanbe_12 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_12")
    p_2shanbe_14 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_14")
    p_2shanbe_16 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_16")
    p_2shanbe_18 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_18")
    p_2shanbe_20 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_2shanbe_20")
    p_3shanbe_08 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_08")
    p_3shanbe_10 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_10")
    p_3shanbe_12 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_12")
    p_3shanbe_14 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_14")
    p_3shanbe_16 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_16")
    p_3shanbe_18 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_18")
    p_3shanbe_20 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_3shanbe_20")
    p_4shanbe_08 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_08")
    p_4shanbe_10 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_10")
    p_4shanbe_12 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_12")
    p_4shanbe_14 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_14")
    p_4shanbe_16 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_16")
    p_4shanbe_18 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_18")
    p_4shanbe_20 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_4shanbe_20")
    p_5shanbe_08 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_08")
    p_5shanbe_10 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_10")
    p_5shanbe_12 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_12")
    p_5shanbe_14 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_14")
    p_5shanbe_16 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_16")
    p_5shanbe_18 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_18")
    p_5shanbe_20 = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="pp_5shanbe_20")

    def __str__(self):
        return self.text + " " + self.professor


class Professor(models.Model):
    unique = models.CharField(max_length=255, blank=False, default="")
    pre_name = models.CharField(max_length=255, blank=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    faculty = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, blank=True)
    note = models.TextField(blank=True)
    status = models.CharField(max_length=255)
    week_plan = models.ForeignKey(WeekPlan, on_delete=models.CASCADE, related_name="week_plan_live")
    image = models.ImageField(blank=True)

    def __str__(self):
        return self.pre_name + " " + self.first_name + " " + self.last_name

# class Getter(models.Model):
#     student_id = models.CharField(max_length=255)
#
#     def __str__(self):
#         return "شماره دانشجویی: " + self.student_id
#
#
# class Schedule(models.Model):
#     professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
#     plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
#     date_added = models.DateTimeField(auto_now_add=True)
#     getter = models.ForeignKey(Getter, on_delete=models.CASCADE)
#
#     def __str__(self):
#         return self.getter
#
