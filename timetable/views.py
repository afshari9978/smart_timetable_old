from django.shortcuts import render
from django.http import HttpResponseRedirect
from timetable.models import Professor, God, WeekPlan, Plan
from utils import weekArray


def index(request):
    professors = Professor.objects.all()
    data = {'professors':professors,}
    return render(request, 'main.html', context=data)


def professors_eng(request, professor_unique):

    professor = Professor.objects.get(unique=professor_unique)
    god = God.objects.get(id=1)
    week_dic = {"shanbe0":weekArray.get()[0], "shanbe1":weekArray.get()[1], "shanbe2":weekArray.get()[2],
                "shanbe3":weekArray.get()[3], "shanbe4":weekArray.get()[4], "shanbe5":weekArray.get()[5],
                "shanbe6":weekArray.get()[6], "today":weekArray.get()[7]}
    data = {'professor': professor, 'week_dic':week_dic, 'god':god}
    return render(request, 'professors_main_page.html', context=data)