from django.conf.urls import url
from timetable import views
from django.urls import path


urlpatterns = [
    path('', views.index),
    path('eng/<str:professor_unique>/', views.professors_eng),

]