#### Todo List
Part 0: Design simple Webpage with simple Django Server<br> 
[X] 01 Design Webpage with 5 parts (logo, pic, table, state, notes) (150p)<br>
[X] 02 Create Database and Server for these 5 parts (80p) <br>
[O] 03 Responsive UI & future edits (150p)<br>
[X] 04 Prepare Server Proxy with Nginx (120p)<br>
[O] 05 Create User management and auth (60p)<br>
[O] 06 Design User Panel (250p)<br>
[O] 07 Create User Panel App and Connect to Main UI (80p)<br>
<br>
#### Members
00 <a href=@afshari9978>Morteza Afshari</a> (200p - 22.4%)<br>
01 <a href=@sooji>Sajjad Beheshti</a> (150p - 16.8%)<br>
<br>
#### Logs
20/05/2017: 0.04 done by 00<br>
19/05/2017: 0.02 done by 01<br>
17/05/2018: 0.01 done by 00<BR>
17/05/2018: todo list edited. total project value: 890p<br>
16/05/2018: project started.<br>


