import locale
from khayyam import *


def find_next_day(today):
    try:
        tomorrow = JalaliDate(today.year, today.month, today.day + 1)
    except ValueError:
        try:
            tomorrow = JalaliDate(today.year, today.month + 1, 1)
        except ValueError:
            tomorrow = JalaliDate(today.year + 1, 1, 1)

    return tomorrow


def create_week_array():
    array = [1, 1, 1, 1, 1, 1, 1, -1]
    today = JalaliDate().today()
    array[int(today.strftime('%w'))] = today.strftime('%n/%P/%K')
    array[7] = int(today.strftime('%w'))
    current = today
    for i in range(6):
        try:
            array[i + int(today.strftime('%w')) + 1] = find_next_day(current).strftime('%n/%P/%K')
        except IndexError:
            array[i + int(today.strftime('%w')) + 1 - 7] = find_next_day(current).strftime('%n/%P/%K')

        current = find_next_day(current)

    return array


def get():
    return create_week_array()